<script src="<?php echo base_url(); ?>assets/js/summernote.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/matmix.init.js"></script>

<div class="row">
					
				<div class="page-breadcrumb">
					<div class="row">
						<div class="col-md-7">
							<div class="page-breadcrumb-wrap">
								<div class="page-breadcrumb-info">
									<h2 class="breadcrumb-titles">Summernote <small>Super Simple WYSIWYG Editor on Bootstrap</small></h2>
									<ul class="list-page-breadcrumb">
										<li><a href="#">Home</a>
										</li>
										<li class="active-page"> Summernote</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-5">
						</div>
					</div>
				</div>
					
					<div class="col-md-12">
						<div class="box-widget widget-module">
							<div class="widget-head clearfix">
								<span class="h-icon"><i class="fa fa-text-width"></i></span>
								<h4>With All Options</h4>
							</div>
							<div class="widget-container">
								<div class="widget-block">
								
									<div class="full-editor">
										In another moment we stop, and all the tired travellers dismount and stretch their cramped limbs. I hear many around me inquiring for hotels or lodgings; but we are expected, and here is our landlady's husband come to meet us; so we hand over our luggage to him, and wend our way to Cliff Cottage. Here we find a smiling hostess, who tells us how glad she is to see us; and after we have removed some of the dust of our journey, we sit down to a well-spread tea-table, on which a noble Cornish pasty holds the post of honour. We{450} draw the table into the bow-window, which faces not directly seawards, but towards the bay, which has been a haven of safety to so many. But it is growing dark already, and we are weary with our long drive; so, soon we seek our fragrant chamber, in which the lavender scents struggle faintly to overpower the pungent aroma of the sea; and it is not long before we are lulled to sleep by the monotonous thunder of the waves on the rocks below.
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>