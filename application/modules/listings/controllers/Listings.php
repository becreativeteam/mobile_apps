<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Listing Module
 * @author      M Arfan
 * @copyright   (c) Dec 2015,  App Development
 * @since       Version 0.1
 */
class Listings extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('listings_model');
        $this->load->model('categories/category_m');
        $this->load->library('form_validation');
    }

    public function index() {
        $data['categories'] = $this->category_m->get_all();
        $data['listings'] = $this->listings_model->get_all();
        $data['view'] = 'listings/home';
        $data['title'] = "Listings";
        $this->load->view('theme/layout', $data);
    }
	
	public function add() { 
		$data['view'] = 'listings/add_listing';
        $data['title'] = "Add Listings";
        $this->load->view('theme/layout', $data);
	}

	
	public function AjaxList() {
        $per_page = $this->input->post('showEntries');
        $page = $this->input->post('page');
        $cur_page = $page;
        $page -= 1;
        $start = $page * $per_page;

        $data = array(
            'title' => 'Listings',
            'page' => $page,
            'cur_page' => $cur_page,
            'per_page' => $per_page,
            'start' => $start,
            'previous_btn' => true,
            'next_btn' => true,
            'first_btn' => true,
            'last_btn' => true,
            'meta' => ($this->input->post('meta')) ? $this->input->post('meta') : 'id',
            'order' => ($this->input->post('order')) ? $this->input->post('order') : 'DESC',
            'searchResults' => $this->input->post('searchResults')
        );

        $Query = $this->listings_model->lists($data, "list");
        $recCount = $this->listings_model->lists($data);
		$data['categories'] = $this->category_m->get_many_by('app_id' , $this->session->userdata('app_id'));
        $data['Query'] = $Query->result();
        $data['recCount'] = $recCount->num_rows();
        $data['view'] = "ajax/list";

        $this->session->set_flashdata('message', $this->input->post('message'));
        $this->load->view('ajaxLayout', $data);
    }

    public function AjaxCall() {
        $action_name = $this->input->post('action_name');
        $column_id = $this->input->post('column_id');
        $id = $this->input->post('id');
        $page_name = $this->input->post('page_name');
        $url = $this->input->post('url');
        $detail = $this->listings_model->get($id);
		
		 if($action_name == "status"){
            if($detail->is_delete == 1){
                $this->listings_model->update($id, array('is_delete' => 0));
                $chek = "<img src=". base_url().'assets/images/accept.png'." title='Click here to deactivate this row' style='margin: 0px 12px;'>";
              
                echo $icon = "<a href='javascript:ajax_call(\"$id\", \"$action_name\", \"$page_name\", \"$column_id\", \"$url\", false)'>".$chek."</a>";
                exit();
            } else {
                $this->listings_model->update($id, array('is_delete' => 1));
                 $chek = "<img src=". base_url().'assets/images/delete.png'." title='Click here to activate this row' style='margin: 0px 12px;'>";
                
                echo $icon = "<a href='javascript:ajax_call(\"$id\", \"$action_name\", \"$page_name\", \"$column_id\", \"$url\", false)'>".$chek."</a>";
                exit();
            }
        }
        
        if ($action_name == "perm_delete") {
            /*if (@getimagesize(FCPATH . '/assets/takeaway/category/' . $detail->cat_image . "_s." . $detail->cat_image_ext)) {
                unlink(FCPATH . '/assets/takeaway/category/' . $detail->cat_image . "_s." . $detail->cat_image_ext);
            }
            if (@getimagesize(FCPATH . '/assets/takeaway/category/' . $detail->cat_image . "_o." . $detail->cat_image_ext)) {
                unlink(FCPATH . '/assets/takeaway/category/' . $detail->cat_image . "_o." . $detail->cat_image_ext);
            }*/
           // echo message_success("Message!", "Row deleted");
            $this->listings_model->delete($id);
        }
    }
	
    public function save($id = '') {
        $id = $this->input->post('id');

        $rules = $this->listings_model->rules_admin;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->listings_model->array_from_post(array('name', 'slug' , 'category_id'));
			$data['app_id'] = $this->session->userdata('app_id');
            if ($id) {
				if (file_exists($_FILES['file1']['tmp_name'])) {
					$cat_image = $this->input->post('image_name');
					$cat_image_ext = $this->input->post('image_ext');

					unlink(FCPATH . '/assets/images/upload/listings/' . $cat_image . "_s." . $cat_image_ext);
					unlink(FCPATH . '/assets/images/upload/listings/' . $cat_image . "_o." . $cat_image_ext);

			
					$img_response = $this->add_image();
					$data['image_name'] = $img_response['name'];
					$data['image_ext'] = $img_response['ext'];
				}
				
                $this->listings_model->update($id, $data, TRUE);
                $this->session->set_flashdata('msg', '<div class="alert alert-success"> Successfully Updated </div>');
            } else {
				if (file_exists($_FILES['file1']['tmp_name'])) {
					$img_response = $this->add_image();
					$data['image_name'] = $img_response['name'];
					$data['image_ext'] = $img_response['ext'];
				}
				
                $this->listings_model->insert($data, TRUE);
                $this->session->set_flashdata('msg', '<div class="alert alert-success"> Successfully Added </div>');
            }
            redirect('listings');
        }
    }

    public function delete($id) {
        $this->listings_model->delete($id);
        $this->session->set_flashdata('msg', '<div class="alert alert-success"> Successfully Deleted </div>');
        redirect('listings');
    }

    public function get_listing_ajax() {
        $id = $this->input->post('id');
        $data = $this->listings_model->get($id);
        echo json_encode($data);
    }
	
	
    /**
     * For adding images
     * @return type
     */
    public function add_image() {
        $time = time() . rand();
        $post_image = array(
            'file' => $_FILES["file1"],
            'new_name' => $time . '_o',
            'dst_path' => FCPATH . '/assets/images/upload/listings/'
        );
        $post_image_response = upload_original($post_image);
        $src_path = FCPATH . '/assets/images/upload/listings/' . $post_image_response['file_name'];

        $thumbs = array(
            array(
                'src_path' => $src_path,
                'dst_path' => FCPATH . '/assets/images/upload/listings/',
                'image_x' => 150,
                'image_y' => 150,
                'image_ratio' => FALSE,
                'quality' => '100%',
                'image_ratio_fill' => FALSE,
                'new_name' => $time . '_s'
            )
        );
        if (empty($cat_image)) {
            foreach ($thumbs as $thumb) {
                upload_resized_images($thumb);
            }
        }
        return array('name' => $time, 'ext' => $post_image_response['file_ext'], 'imagename' => other_asset_url($time . '_s.' . $post_image_response['file_ext'], '', 'uploads/' . $this->config->item('image_dir')));
    }


}
