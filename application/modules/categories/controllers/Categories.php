<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Category Module
 * @author      M Arfan
 * @copyright   (c) Dec 2015,  App Development
 * @since       Version 0.1
 */
class Categories extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('category_m');
        $this->load->model('apps/application_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $data['categories'] = $this->category_m->get_all();
        $data['view'] = 'categories/home';
        $data['title'] = "Categories";
        $this->load->view('theme/layout', $data);
    }
	
	public function AjaxList() {
        $per_page = $this->input->post('showEntries');
        $page = $this->input->post('page');
        $cur_page = $page;
        $page -= 1;
        $start = $page * $per_page;

        $data = array(
            'title' => 'Customers List',
            'page' => $page,
            'cur_page' => $cur_page,
            'per_page' => $per_page,
            'start' => $start,
            'previous_btn' => true,
            'next_btn' => true,
            'first_btn' => true,
            'last_btn' => true,
            'meta' => ($this->input->post('meta')) ? $this->input->post('meta') : 'id',
            'order' => ($this->input->post('order')) ? $this->input->post('order') : 'DESC',
            'searchResults' => $this->input->post('searchResults')
        );

        $Query = $this->category_m->lists($data, "list");
        $recCount = $this->category_m->lists($data);
		$data['apps'] = $this->application_model->get_all();
        $data['Query'] = $Query->result();
        $data['recCount'] = $recCount->num_rows();
        $data['view'] = "ajax/list";

        $this->session->set_flashdata('message', $this->input->post('message'));
        $this->load->view('ajaxLayout', $data);
    }

    public function AjaxCall() {
        $action_name = $this->input->post('action_name');
        $column_id = $this->input->post('column_id');
        $id = $this->input->post('id');
        $page_name = $this->input->post('page_name');
        $url = $this->input->post('url');
        $detail = $this->category_m->get($id);
		
		 if($action_name == "status"){
            if($detail->is_delete == 1){
                $this->category_m->update($id, array('is_delete' => 0));
                $chek = "<img src=". base_url().'assets/images/accept.png'." title='Click here to deactivate this row' style='margin: 0px 12px;'>";
              
                echo $icon = "<a href='javascript:ajax_call(\"$id\", \"$action_name\", \"$page_name\", \"$column_id\", \"$url\", false)'>".$chek."</a>";
                exit();
            } else {
                $this->category_m->update($id, array('is_delete' => 1));
                 $chek = "<img src=". base_url().'assets/images/delete.png'." title='Click here to activate this row' style='margin: 0px 12px;'>";
                
                echo $icon = "<a href='javascript:ajax_call(\"$id\", \"$action_name\", \"$page_name\", \"$column_id\", \"$url\", false)'>".$chek."</a>";
                exit();
            }
        }
        
        if ($action_name == "perm_delete") {
            if (@getimagesize(FCPATH . '/assets/images/upload/categories/' . $detail->image_name . "_s." . $detail->image_ext)) {
                unlink(FCPATH . '/assets/images/upload/categories/' . $detail->image_name . "_s." . $detail->image_ext);
            }
            if (@getimagesize(FCPATH . '/assets/images/upload/categories/' . $detail->image_name . "_o." . $detail->image_ext)) {
                unlink(FCPATH . '/assets/images/upload/categories/' . $detail->image_name . "_o." . $detail->image_ext);
            }
			echo message_success('Message!', "Row deleted");
			
            $this->category_m->delete($id);
        }
		
    }
	
    public function save($id = '') {
        $id = $this->input->post('id');

        $rules = $this->category_m->rules_admin;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->category_m->array_from_post(array('name', 'slug' , 'app_id'));
            if ($id) {
				if (file_exists($_FILES['file1']['tmp_name'])) {
					$image_name = $this->input->post('image_name');
					$image_ext = $this->input->post('image_ext');

					unlink(FCPATH . '/assets/images/upload/categories/' . $image_name . "_s." . $image_ext);
					unlink(FCPATH . '/assets/images/upload/categories/' . $image_name . "_o." . $image_ext);

			
					$img_response = $this->add_image();
					$data['image_name'] = $img_response['name'];
					$data['image_ext'] = $img_response['ext'];
				}
				
                $this->category_m->update($id, $data, TRUE);
                $this->session->set_flashdata('msg', '<div class="alert alert-success"> Successfully Updated </div>');
            } else {
				if (file_exists($_FILES['file1']['tmp_name'])) {
					$img_response = $this->add_image();
					$data['image_name'] = $img_response['name'];
					$data['image_ext'] = $img_response['ext'];
				}
				
                $this->category_m->insert($data, TRUE);
                $this->session->set_flashdata('msg', '<div class="alert alert-success"> Successfully Added </div>');
            }
            redirect('categories');
        }
    }

    public function delete($id) {
        $this->category_m->delete($id);
        $this->session->set_flashdata('msg', '<div class="alert alert-success"> Successfully Deleted </div>');
        redirect('categories');
    }

    public function get_category_ajax() {
        $id = $this->input->post('id');
        $data = $this->category_m->get($id);
        echo json_encode($data);
    }
	
	
    /**
     * For adding images
     * @return type
     */
    public function add_image() {
        $time = time() . rand();
        $post_image = array(
            'file' => $_FILES["file1"],
            'new_name' => $time . '_o',
            'dst_path' => FCPATH . '/assets/images/upload/categories/'
        );
        $post_image_response = upload_original($post_image);
        $src_path = FCPATH . '/assets/images/upload/categories/' . $post_image_response['file_name'];

        $thumbs = array(
            array(
                'src_path' => $src_path,
                'dst_path' => FCPATH . '/assets/images/upload/categories/',
                'image_x' => 150,
                'image_y' => 150,
                'image_ratio' => FALSE,
                'quality' => '100%',
                'image_ratio_fill' => FALSE,
                'new_name' => $time . '_s'
            )
        );
        if (empty($image_name)) {
            foreach ($thumbs as $thumb) {
                upload_resized_images($thumb);
            }
        }
        return array('name' => $time, 'ext' => $post_image_response['file_ext'], 'imagename' => other_asset_url($time . '_s.' . $post_image_response['file_ext'], '', 'uploads/' . $this->config->item('image_dir')));
    }


}
