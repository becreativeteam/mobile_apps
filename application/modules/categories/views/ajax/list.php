<?php 
if($this->session->flashdata('message')){
     echo message_success("Message!", $this->session->flashdata('message'));
}
?>
<script type="text/javascript">
    $(function () {
        $(".edit_category").on("click", function ()
        {
            var value_id = $(this).attr('id');
            var form_data = {
                id: value_id
            };

            $.ajax({
                type: 'POST',
                url: '<?php echo site_url("categories/get_category_ajax"); ?>',
                data: form_data,
                success: function (msg) {
                    var obj = $.parseJSON(msg);
                    $("#name").val(obj['name']);
                    $("#slug").val(obj['slug']);
                    $("#app_id").val(obj['app_id']);
                    $("#image_name").val(obj['image_name']);
                    $("#image_ext").val(obj['image_ext']);
                    $("#id").val(obj['id']);
                    $("#myModalLabel").text("Edit " + obj['name']);
					
                }
            });
            $("#myModal").modal('show');
        });
    });

    function add_category() {
        $("#id").val('');
        $("#image_name").val('');
        $("#image_ext").val('');
        $("#myModalLabel").text("Add New");
    }
	
	
	function string_to_slug(str) {
		  str = str.replace(/^\s+|\s+$/g, ''); // trim
		  str = str.toLowerCase();
		  
		  // remove accents, swap ñ for n, etc
		  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
		  var to   = "aaaaeeeeiiiioooouuuunc------";
		  for (var i=0, l=from.length ; i<l ; i++) {
			str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		  }

		  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
			.replace(/\s+/g, '-') // collapse whitespace and replace by -
			.replace(/-+/g, '-'); // collapse dashes

		  return str;
		}
		
		
		$(document).ready(function() {
			 $("body").on("keyup", "#name", function (){
				var val = string_to_slug($('#name').val());
				$("#slug").val(val);
				return false;
			});
		});
		
		$(document).ready(function() {
			 $("body").on("keyup", "#slug", function (){
				var val = string_to_slug($('#slug').val());
				$("#slug").val(val);
				return false;
			});
		});
			

		
</script>

<div class="box-widget widget-module">
    <div class="widget-head clearfix">
        <span class="h-icon"><i class="fa fa-th"></i></span>
        <h4> <?php echo $title; ?></h4>

        <a href="#"  title="Add New Category" style="float:right; margin:8px 20px 0px 0px;" onclick="add_category();" data-toggle="modal" data-target="#myModal" class="btn btn-default"> Add New </a>

    </div>


    <input type='hidden' name='hiddenShowDataEntries' id='hiddenShowDataEntries' value='<?php echo ($per_page) ? $per_page : 5; ?>' />
    <input type='hidden' name='hiddenSearchResult' id='hiddenSearchResult' value='<?php echo ($searchResults) ? $searchResults : ""; ?>' />
    <input type='hidden' name='hiddenShowMeta' id='hiddenShowMeta' value='<?php echo ($meta) ? $meta : "id"; ?>' />
    <input type='hidden' name='hiddenShowOrder' id='hiddenShowOrder' value='<?php echo ($order) ? $order : "ASC"; ?>' />
    <input type='hidden' name='hiddenPage' id='hiddenPage' value='<?php echo ($page) ? $page : 1; ?>' />

    <div class="widget-container">
        <div class="widget-block">

            <div style="padding:0px;" class="dataTables_length col-md-6">
                <div class="col-md-1" style="padding:10px 0px;"> <b>Show : </b> </div> <select size="1" name="showDataEntries" id="showDataEntries" class="td-select form-control"><option value="10" <?php echo ($per_page == 10) ? "selected" : ''; ?>>10</option><option value="25" <?php echo ($per_page == 25) ? "selected" : ''; ?>>25</option><option value="50" <?php echo ($per_page == 50) ? "selected" : ''; ?>>50</option><option value="100" <?php echo ($per_page == 100) ? "selected" : ''; ?>>100</option><option value="300" <?php echo ($per_page == 300) ? "selected" : ''; ?>>300</option><option value="500" <?php echo ($per_page == 500) ? "selected" : ''; ?>>500</option><option value="1000" <?php echo ($per_page == 1000) ? "selected" : ''; ?>>1000</option></select>
            </div>

            <div style="padding:0px;" class="dataTables_filter col-md-6">

                Search : <input type="search" id='searchResult' class="form-control input-md" value='<?php echo $searchResults ?>' placeholder='Search' autofocus='true'> 

            </div>



            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="">
                            <a class="sortOrder" data-meta="id" data-order="ASC" data-title="ID" title="Sort id ASC" style="cursor:pointer;"><img src="<?php echo base_url(); ?>/assets/images/sort_desc.png"></a>
                            ID
                            <a class="sortOrder" data-meta="id" data-order="DESC" data-title="ID" title="Sort id DESC" style="cursor:pointer;"><img class="desc" src="<?php echo base_url(); ?>/assets/images/sort_asc.png"></i></a>
                        </th>
						<th>Image  </th>
                        <th width="">
                            <a class="sortOrder" data-meta="name" data-order="ASC" data-title="Name" title="Sort name ASC" style="cursor:pointer;"><img src="<?php echo base_url(); ?>/assets/images/sort_desc.png"></a>
                            Name
                            <a class="sortOrder" data-meta="name" data-order="DESC" data-title="Name" title="Sort name DESC" style="cursor:pointer;"><img class="desc" src="<?php echo base_url(); ?>/assets/images/sort_asc.png"></i></a>
                        </th>

                        <th width="">
                            <a class="sortOrder" data-meta="slug" data-order="ASC" data-title="Name" title="Sort name ASC" style="cursor:pointer;"><img src="<?php echo base_url(); ?>/assets/images/sort_desc.png"></a>
                            Slug
                            <a class="sortOrder" data-meta="slug" data-order="DESC" data-title="Name" title="Sort name DESC" style="cursor:pointer;"><img class="desc" src="<?php echo base_url(); ?>/assets/images/sort_asc.png"></i></a>
                        </th>


                        <th>Status </th>
                        <th>Option</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if ($recCount > 0) {

						$i = 1;
                        foreach ($Query as $row) {
								$i++;
								
								if($i%2 == 0)
								$class = "even";
								else 
								$class = "odd";
						
										$id = $row->id;
                                        $action_is_active = "status";
                                        $action_temp_delete = "temp_delete"; 
                                        $action_perm_delete = "perm_delete";
                                        $row_id = "row_".$id;
                                        $req_page = "category_page";
                                        $column_status = "column_status";
                                        $column_action = "column_action";
                                        $url = site_url("categories/AjaxCall");
						
										if($row->is_delete == 0){
                                            $chek = "<img src=". base_url().'assets/images/accept.png'." title='Click here to deactivate this row' style='margin: 0px 12px;'>"; 
                                        } else {
                                            $chek = "<img src=". base_url().'assets/images/delete.png'." title='Click here to activate this row' style='margin: 0px 12px;'>";
                                        }

                                        $is_active = "<div id='".$column_status.$id."'><a href='javascript:ajax_call(".$id.", \"$action_is_active\", \"$req_page\", \"$column_status\", \"$url\", false)'>".$chek."</a></div>";
                                        $is_delete = "<a href='javascript:ajax_call(".$id.", \"$action_perm_delete\", \"$req_page\", \"$column_action\", \"$url\", \"$row_id\")' onclick=\"return confirm('Are you sure, you want to delete this row permanently ? ');\" title='Click To Delete'><i class='fa fa-trash-o fa-sm'></i></a>";
                                   
								   
                            ?>
                            <tr  id="<?php echo $row_id ?>" class="<?php echo $class; ?>">
                                <td ><?php echo $row->id; ?></td>
                                <td ><img width="50px" height="50px" src="<?php echo base_url(); ?>assets/images/upload/categories/<?php echo $row->image_name."_s.". $row->image_ext; ?>"></td>
                                <td><a class="edit_category" href="#" id="<?php echo $row->id; ?>"> <?php echo $row->name; ?></td>
                                <td><?php echo $row->slug; ?></td>
                                <td><?php echo $is_active; ?></td>

                                <td>
                                    <a class="edit_category" href="#" id="<?php echo $row->id; ?>"> <i class="fa fa-edit"></i></a> 
									 <?php echo $is_delete; ?>
                                </td> 
                            </tr>
                            <?php
                        }
                    }
                    ?>

                </tbody>
            </table>

            <div class="row">

                <div id="ataTables_Table_1_paginate" class="dataTables_paginate paging_simple_numbers">
                    <tr>
                        <td>
                            <?php
                            $no_of_paginations = ceil($recCount / $per_page);
                            echo getPagination($recCount, $no_of_paginations, $cur_page, $first_btn, $last_btn, $previous_btn, $next_btn);
                            ?>
                        </td>
                    </tr>
                </div>
            </div>
        </div>
    </div>
</div> 


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="bootbox-close-button close" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel" >Add New</h4></div>
            <?php echo form_open_multipart('categories/save'); ?>
            <div class="modal-body">
                <div class="bootbox-body">
                    <div class="row"> 
                        <div class="col-md-12"> 

                            <div class="form-group form-text-margin">
                                <label class="col-md-3 control-label form-label-padding" for="inputDefault"> Name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" required  id="name" name="name" required value="" />
                                </div>
                            </div>
							<div class="clearfix"> </div>

                            <div class="form-group form-text-margin">
                                <label class="col-md-3 control-label form-label-padding" for="inputDefault"> Slug</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  required id="slug" name="slug" required value="" />
                                    <input type="hidden" class="form-control" id="id" name="id"  value="" />
                                </div>
                            </div>
							<div class="clearfix"> </div>
							
							 <div class="form-group form-text-margin">
                                <label class="col-md-3 control-label form-label-padding" for="inputDefault"> Application  </label>
                                <div class="col-md-6">
									<select class="form-control" name="app_id" id="app_id" > 
										<?php foreach($apps as $list) { ?>
											<option value="<?php echo $list->id; ?>"> <?php echo $list->name; ?> </option>
										<?php } ?>
									</select>
                                </div>
                            </div>
							<div class="clearfix"> </div>
							
							<input type="hidden" value="" name="image_name" id="image_name" />
							<input type="hidden" value="" name="image_ext" id="image_ext" />
							 <div class="form-group form-text-margin">
								<label class="col-md-3 control-label form-label-padding" for="inputDefault"> Image</label>
								<div class="col-md-6 form-label-padding">
									<input type="file" id="file1" name="file1" >
								</div>
							</div>
							
							<div class="clearfix"> </div>
							
							
                        </div> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" data-bb-handler="success">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>


