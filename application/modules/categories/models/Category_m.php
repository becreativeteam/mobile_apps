<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Category Module
 * @author      M Arfan
 * @copyright   (c) Dec 2015,  App Development
 * @since       Version 0.1
 */
class Category_m extends MY_Model {

    protected $primary_key = "id";
    protected $_table = "categories";


    /* protected $has_many = array(
      'questions',
      'answers'); */
    public $rules_admin = array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required|trim')
    );

    public function __construct() {
        parent::__construct();
    }

    public function get_new() {
        $user = new stdClass();
        $user->name = '';
        $user->slug = '';
        return $user;
    }
	
	  public function lists($array = "", $type = "") {
        $searchResults = $array['searchResults'];
        $meta = $array['meta'];
        $order = $array['order'];
        $per_page = $array['per_page'];
        $start = $array['start'];
        $this->db->select('*');
		$this->db->where('app_id' , $this->session->userdata('app_id'));
        $this->db->from('categories');

        if (isset($searchResults) && $searchResults != "") {
           $this->db->where("(`name` LIKE '%$searchResults%' OR `slug` LIKE '%$searchResults%')");
        }
        if ($type == "list") {
            $this->db->limit($per_page, $start);
            $this->db->order_by($meta, $order);
        }

        $query = $this->db->get();
        return $query;
    }

}
