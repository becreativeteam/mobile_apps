<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Application Module
 * @author      M Arfan
 * @copyright   (c) Dec 2015,  App Development
 * @since       Version 0.1
 */
class Apps extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('application_model');
        $this->load->model('admin/admin_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $data['applications'] = $this->application_model->get_all();
        $data['view'] = 'apps/home';
        $data['title'] = "Applications";
        $this->load->view('theme/layout', $data);
    }
	
	public function AjaxList() {
        $per_page = $this->input->post('showEntries');
        $page = $this->input->post('page');
        $cur_page = $page;
        $page -= 1;
        $start = $page * $per_page;

        $data = array(
            'title' => 'Applications List',
            'page' => $page,
            'cur_page' => $cur_page,
            'per_page' => $per_page,
            'start' => $start,
            'previous_btn' => true,
            'next_btn' => true,
            'first_btn' => true,
            'last_btn' => true,
            'meta' => ($this->input->post('meta')) ? $this->input->post('meta') : 'id',
            'order' => ($this->input->post('order')) ? $this->input->post('order') : 'DESC',
            'searchResults' => $this->input->post('searchResults')
        );

        $Query = $this->application_model->lists($data, "list");
        $recCount = $this->application_model->lists($data);
		
		$data['users'] = $this->admin_model->get_all();
        $data['Query'] = $Query->result();
        $data['recCount'] = $recCount->num_rows();
        $data['view'] = "ajax/list";

        $this->session->set_flashdata('message', $this->input->post('message'));
        $this->load->view('ajaxLayout', $data);
    }

    public function AjaxCall() {
        $action_name = $this->input->post('action_name');
        $column_id = $this->input->post('column_id');
        $id = $this->input->post('id');
        $page_name = $this->input->post('page_name');
        $url = $this->input->post('url');
        $detail = $this->application_model->get($id);
		
		 if($action_name == "status"){
            if($detail->is_delete == 1){
                $this->application_model->update($id, array('is_delete' => 0));
                $chek = "<img src=". base_url().'assets/images/accept.png'." title='Click here to deactivate this row' style='margin: 0px 12px;'>";
              
                echo $icon = "<a href='javascript:ajax_call(\"$id\", \"$action_name\", \"$page_name\", \"$column_id\", \"$url\", false)'>".$chek."</a>";
                exit();
            } else {
                $this->application_model->update($id, array('is_delete' => 1));
                 $chek = "<img src=". base_url().'assets/images/delete.png'." title='Click here to activate this row' style='margin: 0px 12px;'>";
                
                echo $icon = "<a href='javascript:ajax_call(\"$id\", \"$action_name\", \"$page_name\", \"$column_id\", \"$url\", false)'>".$chek."</a>";
                exit();
            }
        }
        
        if ($action_name == "perm_delete") {
            /*if (@getimagesize(FCPATH . '/assets/takeaway/category/' . $detail->cat_image . "_s." . $detail->cat_image_ext)) {
                unlink(FCPATH . '/assets/takeaway/category/' . $detail->cat_image . "_s." . $detail->cat_image_ext);
            }
            if (@getimagesize(FCPATH . '/assets/takeaway/category/' . $detail->cat_image . "_o." . $detail->cat_image_ext)) {
                unlink(FCPATH . '/assets/takeaway/category/' . $detail->cat_image . "_o." . $detail->cat_image_ext);
            }*/
           // echo message_success("Message!", "Row deleted");
            $this->application_model->delete($id);
        }
		
    }
	
    public function save($id = '') {
        $id = $this->input->post('id');

        $rules = $this->application_model->rules_admin;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->application_model->array_from_post(array('name' , 'user_id'));
            if ($id) {
                $this->application_model->update($id, $data, TRUE);
                $this->session->set_flashdata('msg', '<div class="alert alert-success"> Successfully Updated </div>');
            } else {
				
                $this->application_model->insert($data, TRUE);
                $this->session->set_flashdata('msg', '<div class="alert alert-success"> Successfully Added </div>');
            }
            redirect('apps');
        }
    }

    public function delete($id) {
        $this->application_model->delete($id);
        $this->session->set_flashdata('msg', '<div class="alert alert-success"> Successfully Deleted </div>');
        redirect('apps');
    }

    public function get_app_ajax() {
        $id = $this->input->post('id');
        $data = $this->application_model->get($id);
        echo json_encode($data);
    }

}
