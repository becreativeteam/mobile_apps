<script type="text/javascript">
    window.showEntries = 10;
    window.page = 1;
    window.pageUrlLoad = "<?php echo site_url("apps/AjaxList"); ?>";
</script>

<script src="<?php echo base_url(); ?>assets/js/ajax-datatable-custom/blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ajax-datatable-custom/adminPanel.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ajax-datatable-custom/adminCommonFunction.js"></script>

<div class="page-breadcrumb">
					<div class="row">
						<div class="col-md-7">
							<div class="page-breadcrumb-wrap">
								<div class="page-breadcrumb-info">
									<h2 class="breadcrumb-titles"><?php echo $title; ?> </h2>
									<ul class="list-page-breadcrumb">
										<li><a href="<?php echo base_url(); ?>">Home</a>
										</li>
										<li class="active-page"> <?php echo $title; ?></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-5">
						
						</div>
					</div>
				</div>
				
    <!--start container-->
   <div class="row">
					<div class="col-md-12">


        <div id="ajax_message_jgrowl"></div>

        <!-- Panels Start -->
        <div id="loadPageData">
            <div id="ajaxloader"><img src="<?php echo base_url(); ?>assets/bigloader.gif" /></div>
        </div>

        <div id="loadPageDataAddForm" style="display: none;">
            <div id="ajaxloader"></div>
        </div>

        <div id="loadPageDataEditForm" style="display: none;">
            <div id="ajaxloader"></div>
        </div>
        <!-- Panels End -->


    </div> 
    </div> 
