<script type="text/javascript">
    $(function () {
        $(".edit_category").on("click", function ()
        {
            var value_id = $(this).attr('id');
            var form_data = {
                id: value_id
            };

            $.ajax({
                type: 'POST',
                url: '<?php echo site_url("apps/get_app_ajax"); ?>',
                data: form_data,
                success: function (msg) {
                    var obj = $.parseJSON(msg);
                    $("#name").val(obj['name']);
         
                    $("#id").val(obj['id']);
                    $("#myModalLabel").text("Edit " + obj['name']);
					
                }
            });
            $("#myModal").modal('show');
        });
    });

    function add_category() {
        $("#id").val('');
        $("#name").val('');
        $("#myModalLabel").text("Add New");
    }
	
		
</script>

<div class="box-widget widget-module">
    <div class="widget-head clearfix">
        <span class="h-icon"><i class="fa fa-th"></i></span>
        <h4><?php echo $title; ?></h4>

        <a href="#"  title="Add New Category" style="float:right; margin:8px 20px 0px 0px;" onclick="add_category();" data-toggle="modal" data-target="#myModal" class="btn btn-default"> Add New </a>

    </div>


    <input type='hidden' name='hiddenShowDataEntries' id='hiddenShowDataEntries' value='<?php echo ($per_page) ? $per_page : 5; ?>' />
    <input type='hidden' name='hiddenSearchResult' id='hiddenSearchResult' value='<?php echo ($searchResults) ? $searchResults : ""; ?>' />
    <input type='hidden' name='hiddenShowMeta' id='hiddenShowMeta' value='<?php echo ($meta) ? $meta : "id"; ?>' />
    <input type='hidden' name='hiddenShowOrder' id='hiddenShowOrder' value='<?php echo ($order) ? $order : "ASC"; ?>' />
    <input type='hidden' name='hiddenPage' id='hiddenPage' value='<?php echo ($page) ? $page : 1; ?>' />

    <div class="widget-container">
        <div class="widget-block">

            <div style="padding:0px;" class="dataTables_length col-md-6">
                <div class="col-md-1" style="padding:10px 0px;"> <b>Show : </b> </div> <select size="1" name="showDataEntries" id="showDataEntries" class="td-select form-control"><option value="10" <?php echo ($per_page == 10) ? "selected" : ''; ?>>10</option><option value="25" <?php echo ($per_page == 25) ? "selected" : ''; ?>>25</option><option value="50" <?php echo ($per_page == 50) ? "selected" : ''; ?>>50</option><option value="100" <?php echo ($per_page == 100) ? "selected" : ''; ?>>100</option><option value="300" <?php echo ($per_page == 300) ? "selected" : ''; ?>>300</option><option value="500" <?php echo ($per_page == 500) ? "selected" : ''; ?>>500</option><option value="1000" <?php echo ($per_page == 1000) ? "selected" : ''; ?>>1000</option></select>
            </div>

            <div style="padding:0px;" class="dataTables_filter col-md-6">

                Search : <input type="search" id='searchResult' class="form-control input-md" value='<?php echo $searchResults ?>' placeholder='Search' autofocus='true'> 

            </div>


            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="">
                            <a class="sortOrder" data-meta="id" data-order="ASC" data-title="ID" title="Sort id ASC" style="cursor:pointer;"><img src="<?php echo base_url(); ?>/assets/images/sort_desc.png"></a>
                            ID
                            <a class="sortOrder" data-meta="id" data-order="DESC" data-title="ID" title="Sort id DESC" style="cursor:pointer;"><img class="desc" src="<?php echo base_url(); ?>/assets/images/sort_asc.png"></i></a>
                        </th>
						
                        <th width="">
                            <a class="sortOrder" data-meta="name" data-order="ASC" data-title="Name" title="Sort name ASC" style="cursor:pointer;"><img src="<?php echo base_url(); ?>/assets/images/sort_desc.png"></a>
                            Name
                            <a class="sortOrder" data-meta="name" data-order="DESC" data-title="Name" title="Sort name DESC" style="cursor:pointer;"><img class="desc" src="<?php echo base_url(); ?>/assets/images/sort_asc.png"></i></a>
                        </th>
                        <th>Status</th>
                        <th>Option</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if ($recCount > 0) {


                        foreach ($Query as $row) {
						
										$id = $row->id;
                                        $action_is_active = "status";
                                        $action_temp_delete = "temp_delete"; 
                                        $action_perm_delete = "perm_delete";
                                        $row_id = "row_".$id;
                                        $req_page = "category_page";
                                        $column_status = "column_status";
                                        $column_action = "column_action";
                                        $url = site_url("apps/AjaxCall");
						
										if($row->is_delete == 0){
                                            $chek = "<img src=". base_url().'assets/images/accept.png'." title='Click here to deactivate this row' style='margin: 0px 12px;'>"; 
                                        } else {
                                            $chek = "<img src=". base_url().'assets/images/delete.png'." title='Click here to activate this row' style='margin: 0px 12px;'>";
                                        }

                                        $is_active = "<div id='".$column_status.$id."'><a href='javascript:ajax_call(".$id.", \"$action_is_active\", \"$req_page\", \"$column_status\", \"$url\", false)'>".$chek."</a></div>";
                                        $is_delete = "<a href='javascript:ajax_call(".$id.", \"$action_perm_delete\", \"$req_page\", \"$column_action\", \"$url\", \"$row_id\")' onclick=\"return confirm('Are you sure, you want to delete this row permanently ? ');\" title='Click To Delete'><i class='fa fa-trash-o fa-sm'></i></a>";
                                   
								   
                            ?>
                            <tr id="<?php echo $row_id ?>">
                                <td ><?php echo $row->id; ?></td>
                                <td><a class="edit_category" href="#" id="<?php echo $row->id; ?>"> <?php echo $row->name; ?></td>
                                <td><?php echo $is_active; ?></td>

                                <td>
                                    <a class="edit_category" href="#" id="<?php echo $row->id; ?>"> <i class="fa fa-edit"></i></a> 
									 <?php echo $is_delete; ?>
                                </td> 
                            </tr>
                            <?php
                        }
                    }
                    ?>

                </tbody>
            </table>

            <div class="row">

                <div id="ataTables_Table_1_paginate" style="margin-top:30px"  class="dataTables_paginate paging_simple_numbers">
                    <tr>
                        <td>
                            <?php
                            $no_of_paginations = ceil($recCount / $per_page);
                            echo getPagination($recCount, $no_of_paginations, $cur_page, $first_btn, $last_btn, $previous_btn, $next_btn);
                            ?>
                        </td>
                    </tr>
                </div>
            </div>
        </div>
    </div>
</div> 


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="bootbox-close-button close" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel" >Add New</h4></div>
            <?php echo form_open_multipart('apps/save'); ?>
            <div class="modal-body">
                <div class="bootbox-body">
                    <div class="row"> 
                        <div class="col-md-12"> 

                            <div class="form-group form-text-margin">
                                <label class="col-md-3 control-label form-label-padding" for="inputDefault"> Name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" required  id="name" name="name" required value="" />
									 <input type="hidden" class="form-control" id="id" name="id"  value="" />
                                </div>
                            </div>
							<div class="clearfix"> </div>
							
							<div class="form-group form-text-margin">
                                <label class="col-md-3 control-label form-label-padding" for="inputDefault"> User </label>
                                <div class="col-md-6">
									<select class="form-control" name="user_id" id="user_id" > 
										<?php foreach($users as $list) { ?>
											<option value="<?php echo $list->id; ?>"> <?php echo $list->username; ?> </option>
										<?php } ?>
									</select>
                                </div>
                            </div>
							<div class="clearfix"> </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" data-bb-handler="success">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>


