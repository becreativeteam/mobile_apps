<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard_m');
    }

    public function index() {
        
        $data['view'] = "dashboard/home";
        $data['title'] = "Dashboard";
        $this->load->view('theme/layout', $data);
    }
    
  
}
