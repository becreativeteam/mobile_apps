<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Controller
 * @author      M Arfan
 * @copyright   (c) 2014 CMS
 */
class Admin extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('form_validation');
    }

    public function index() {
        
		if ($this->session->userdata('is_loggedin')) {
            redirect('dashboard');
        }
		// Apply login rules 
        $rules = $this->admin_model->rules_login;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            // if validation true validate username and password
            if ($this->admin_model->login()) {
				$this->set_default_app($this->session->userdata('user_id'));
                redirect("dashboard");
            } else {
                // login failed 
                $this->session->set_flashdata('error', 'That username/password combination does not exist');
                redirect('admin', 'refresh');
            }
        }
        $this->load->view('admin/login');
    }
	
	public function set_default_app($user_id) { 
		$query = $this->db->where('user_id' , $user_id)
						  ->get('apps')
						  ->row();
		$this->session->set_userdata('app_id' , $query->id);
		return TRUE;
	}

    public function logout() {
        $this->admin_model->logout();
        redirect('admin');
    }

}
