<!doctype html>
<html>

<!-- Mirrored from lab.westilian.com/matmix/iconic-view/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Dec 2015 10:46:07 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="A Components Mix Bootstarp 3 Admin Dashboard Template">
<meta name="author" content="Westilian">
<title>MatMix - A Components Mix Admin Dashboard Template</title>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/waves.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/layout.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/components.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/common-styles.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/pages.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/matmix-iconfont.css" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Roboto:400,300,400italic,500,500italic" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" type="text/css">
</head>
<body class="login-page">
    <div class="page-container">
        <div class="login-branding">
            <a href="index-2.html"><img src="<?php echo base_url(); ?>assets/images/logo-large.png" alt="logo"></a>
        </div>
        <div class="login-container">
            <img class="login-img-card" src="<?php echo base_url(); ?>assets/images/avatar/jaman-01.jpg" alt="login thumb" />
						<?php echo $this->session->flashdata('error'); ?>
                        <?php echo validation_errors(); ?>
                        <?php echo form_open('admin/index'); ?>
			
                <input type="text" name="username" id="inputEmail" class="form-control floatlabel " placeholder="Username" required autofocus>
                <input type="password" name="password" id="inputPassword" class="form-control floatlabel " placeholder="Password" required>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" class="switch-mini" /> Remember Me
                    </label>
                </div>
                <button class="btn btn-primary btn-block btn-signin" type="submit">Sign In</button>
            </form>

            <a href="#" class="forgot-password">
                Forgot the password?
            </a>
        </div>
        <div class="create-account">
            <a href="#">
                Create Account
            </a>
        </div>

        <div class="login-footer">
            Crafted with<i class="fa fa-heart"></i>by <a href="#">westilian</a>

        </div>

    </div>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jRespond.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nav-accordion.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/hoverintent.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/switchery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.loadmask.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/icheck.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootbox.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/animation.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/colorpicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/floatlabels.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/smart-resize.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/layout.init.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/matmix.init.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/retina.min.js"></script>
</body>


<!-- Mirrored from lab.westilian.com/matmix/iconic-view/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Dec 2015 10:46:09 GMT -->
</html>