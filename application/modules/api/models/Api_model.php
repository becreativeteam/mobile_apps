<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package      API Module
 * @author      Becreative Group 
 * @copyright   (c) 2015, CMS Development
 * @since       Version 0.1
 */
class Api_model extends MY_Model {

  

    public function __construct() {
        parent::__construct();
    }

    public function app_categories($app_id) { 
		$this->db->where('app_id' , $app_id);
		$query = $this->db->get('categories');
		foreach($query->result() as $key=> $row) { 
			$row->image_s = base_url(). "assets/images/upload/categories/".$row->image_name."_s.".$row->image_ext;
			$row->image_o = base_url(). "assets/images/upload/categories/".$row->image_name."_o.".$row->image_ext;
		}
		return $query->result();
	}
	
	public function category_listings($category_id) { 
		$this->db->where('category_id' , $category_id);
		$query = $this->db->get('listings');
		foreach($query->result() as $key=> $row) { 
			$row->image_s = base_url(). "assets/images/upload/listings/".$row->image_name."_s.".$row->image_ext;
			$row->image_o = base_url(). "assets/images/upload/listings/".$row->image_name."_o.".$row->image_ext;
		}
		return $query->result();
	}
	
	public function single_listings($listing_id) { 
		$this->db->where('id' , $listing_id);
		$query = $this->db->get('listings');
		$row = $query->row();
			$row->image_s = base_url(). "assets/images/upload/listings/".$row->image_name."_s.".$row->image_ext;
			$row->image_o = base_url(). "assets/images/upload/listings/".$row->image_name."_o.".$row->image_ext;
		
		return $query->row();
	}
}
