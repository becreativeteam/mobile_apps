<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
/**
 * @package      Categories Controller
 * @company       BeCreative Group
 * @copyright   (c) 2016 mobile Apps 
 */
class Api extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('categories/category_m');
        $this->load->model('apps/application_model');
        $this->load->model('api_model');
        $this->load->model('listings/listings_model');
        $this->load->library('form_validation');
    }
	
	public function app_categories_get() { 
	
		
		
		
		if(!$this->get('app_id')) { 
			$this->response(array('status' => FALSE, 'error' => 'App ID missing'), 201);
		} else { 
			$app_id = $this->get('app_id');
		}
		if(!$this->application_model->get($app_id)) { 
			$this->response(array('status' => FALSE, 'error' => 'Application not found!'), 201);
		}
		
		/*$this->load->library('pagination');
        $config['base_url'] = base_url() . '?page=';
        $config['total_rows'] = count($this->api_model->app_categories($app_id));
        $config['per_page'] = 0;
        $config['num_links'] = 4;
        $config['uri_segment'] = $this->input->get('page'); */
       
        //$this->pagination->initialize($config);
		
		$data = $this->api_model->app_categories($app_id);
		
		if($data)
        {
            $this->response(array('status' => TRUE, 'data' => $data), 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('status' => FALSE, 'error' => 'Result Not Found'), 404);
        }
	}
	
	public function category_listings_get() { 
		
		if(!$this->get('category_id')) { 
			$this->response(array('status' => FALSE, 'error' => 'Category ID missing'), 201);
		} else { 
			$category_id = $this->get('category_id');
		}
		if(!$this->category_m->get($category_id)) { 
			$this->response(array('status' => FALSE, 'error' => 'Category not found!'), 201);
		}
		
		$data = $this->api_model->category_listings($category_id);
		
		if($data)
        {
            $this->response(array('status' => TRUE, 'data' => $data), 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('status' => FALSE, 'error' => 'Result Not Found'), 404);
        }
	}
	
	public function listing_get() { 
		
		if(!$this->get('listing_id')) { 
			$this->response(array('status' => FALSE, 'error' => 'Listing_id ID missing'), 201);
		} else { 
			$listing_id = $this->get('listing_id');
		}
		if(!$this->listings_model->get($listing_id)) { 
			$this->response(array('status' => FALSE, 'error' => 'Listings not found!'), 201);
		}
		
		$data = $this->api_model->single_listings($listing_id);
		
		if($data)
        {
            $this->response(array('status' => TRUE, 'data' => $data), 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('status' => FALSE, 'error' => 'Result Not Found'), 404);
        }
	}


}
