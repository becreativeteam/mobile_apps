<!doctype html>
<html>

<head>
    <?php $this->load->view('theme/partials/head') ?>
</head>

<body>
<div class="page-container iconic-view">
	  <!-- START LEFT SIDEBAR NAV-->
      <?php $this->load->view('theme/partials/left_sidebar'); ?>
      <!-- END LEFT SIDEBAR NAV-->
	  <div class="page-content">
  <!-- START HEADER -->
   <?php $this->load->view('theme/partials/header'); ?>
  <!-- END HEADER -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->
<div class="main-container">
<div class="container-fluid">
    

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <?php $this->load->view($view);?>
      <!-- END CONTENT -->
        <!-- Floating Action Button -->
     
 
  </div>
  </div>
  <!-- END MAIN -->


<?php $this->load->view('theme/partials/footer'); ?>
 </div>
  </div>
    <?php $this->load->view('theme/partials/right_sidebar'); ?>


<script src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jRespond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/nav-accordion.js"></script>
<script src="<?php echo base_url(); ?>assets/js/hoverintent.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/switchery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.loadmask.js"></script>
<script src="<?php echo base_url(); ?>assets/js/icheck.js"></script>
<script src="<?php echo base_url(); ?>assets/js/select2.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-filestyle.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootbox.js"></script>
<script src="<?php echo base_url(); ?>assets/js/animation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sweetalert.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
<script src="<?php echo base_url(); ?>assets/js/calendar/fullcalendar.js"></script>
<?php if($this->uri->segment(1) == "dashboard") {  ?>
<script src="<?php echo base_url(); ?>assets/js/layout.init.js"></script>
<?php } ?>



</body>

<!-- Mirrored from lab.westilian.com/matmix/iconic-view/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Dec 2015 10:41:57 GMT -->
</html>