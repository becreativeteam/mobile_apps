
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="mobile applications">

    <title>Mobile Applications</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/waves.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/layout.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/components.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/common-styles.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/pages.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/matmix-iconfont.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/toastr.min.css" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,300,400italic,500,500italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" type="text/css">
	
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/toastr.min.js"></script>