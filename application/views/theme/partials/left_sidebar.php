<div class="left-aside desktop-view">
    <div class="aside-branding">
        <a href="<?php echo base_url(); ?>" class="iconic-logo"><img src="<?php echo base_url(); ?>assets/images/logo-iconic.png" alt="Matmix Logo">
        </a>
        <a href="<?php echo base_url(); ?>" class="large-logo"><img src="<?php echo base_url(); ?>assets/images/logo-large.png" alt="Matmix Logo">
        </a><span class="aside-pin waves-effect"><i class="fa fa-thumb-tack"></i></span>
        <span class="aside-close waves-effect"><i class="fa fa-times"></i></span>
    </div>
    <div class="left-navigation">
        <ul class="list-accordion">
            <li><a href="<?php echo base_url(); ?>dashboard" class="waves-effect"><span class="nav-icon"><i class="fa fa-home"></i></span><span class="nav-label">Dashboard</span> </a> </li>
            <li><a href="<?php echo base_url(); ?>apps" class="waves-effect"><span class="nav-icon"><i class="fa fa-align-justify"></i></span><span class="nav-label">Applications</span></a></li>
            <li><a href="<?php echo base_url(); ?>categories" class="waves-effect"><span class="nav-icon"><i class="fa fa-align-justify"></i></span><span class="nav-label">Categories</span></a></li>
            <li><a href="<?php echo base_url(); ?>listings" class="waves-effect"><span class="nav-icon"><i class="fa fa-align-justify"></i></span><span class="nav-label">Listings</span></a></li>
        </ul>
    </div>
</div>